# drake-gitlab-docker-example

This is an example/template repository for building `drake` R projects on GitLab CI using Docker. It builds off the drake [`mtcars` example](https://github.com/wlandau/drake-examples/tree/master/mtcars).

Some key features:

-   The main build script is `make.R`, which builds the project in the local environment.  
-   The build environment is defined in the project `Dockerfile` and packages required are in `DESCRIPTION`.
-   Two jobs are defined in `.gitlab-ci.yml`. The first builds or updates the Docker image and is run only when `Dockerfile` or `DESCRIPTION` change.  The image is stored in GitLab's project-level image registry.  The second job builds the drake pipeline.
-   Outputs from the drake pipeline (model objects, compiled reports, and a drake network graph) go into `artifacts` when built.  This directory is stored as an artifact with each GitLab build job, but `.gitignore`'d.
-   A [metrics file](https://docs.gitlab.com/ee/ci/metrics_reports.html) is saved
    on each run which can be read and reported by GitLab.
-   The `.drake` directory is `.gitignore`'d but cached on GitLab between jobs so as to minimize build times.  It is also stored as a     an artifact (`artifacts/drake_cache.zip) so it may be downloaded and re-used.
    -   An experimental function in `R/get_cache.R` imports the drake cache from GitLab CI into the local drake cache.  It requires a GitLab access token to be set a `GITLAB_PAT` in the environment (e.g., in `.Renviron`).
-   A `Makefile` defines a docker command so that `make` builds the Docker environment and runs the `drake` pipeline inside it. This facilitates running the dockerized pipeline locally.
-   The RStudio project file (`.Rproj`) is configured so that `make.R` is run using "Build All" (Cmd+Shift+B) in the Rstudio build pane.

---

Please note that this project is released with a
[Contributor Code of Conduct](CODE_OF_CONDUCT.md).
By contributing to this project, you agree to abide by its terms.